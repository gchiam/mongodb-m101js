use zips
db.zips.aggregate([
    {
        $project: {
            _id: 0,
            first_char: {
                $substr : [ "$city", 0, 1]
            },
            pop: 1
        },
    },
    {
        $match: { $and: [ { first_char: { $gte: '0' } }, {first_char: { $lte: '9' } } ] }
    },
    {
        $group: {
            _id: null,
            total_pop: { $sum: "$pop" }
        }
    }
])
