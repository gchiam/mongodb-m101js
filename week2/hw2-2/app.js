var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/weather', function(err, db) {
    if (err) throw err;

    var collection = db.collection('data');
    var query = {};
    var options = {
        sort: [['State', 1], ['Temperature', -1]]
    };

    var cursor = collection.find(query, {}, options);
    var prevState = null;

    function updateHighestTemperature(doc) {
        doc.month_high = true;
        console.log('trying to update ');
        collection.save(doc, function(err, saved) {
            if (err) throw err;

            console.log('Successfully updated ' + saved + ' document.');
        });
    };

    function eachCallback(err, doc) {
        if (err) throw err;

        if (doc === null) {
            return db.close();
        }

        if (prevState !== doc.State) {
            console.log(doc.State + ' ' + doc.Temperature);
            prevState = doc.State;
            updateHighestTemperature(doc);
        }
    };

    cursor.each(eachCallback);
});
