var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/students', function(err, db) {
    if (err) throw err;

    var collection = db.collection('students');
    var query = {};
    var fields = { scores: true };
    var options =  {};

    var cursor = collection.find(query, fields,  options);

    function removeLowestHomework(doc) {
        var scores = doc.scores||null;
        console.log('before:');
        console.log(scores);
        if (scores) {
            var lowHomeworkScoreIndex = -1;
            var lowHomeworkScore = 100;
            scores.forEach(function(score, index) {
                if ((score.type === 'homework') && (score.score < lowHomeworkScore)) {
                    lowHomeworkScore = score.score;
                    lowHomeworkScoreIndex = index;
                }
            });
            if (lowHomeworkScoreIndex >= 0) {
                scores.splice(lowHomeworkScoreIndex, 1);
                console.log('removed ' + lowHomeworkScore + ' at index ' + lowHomeworkScoreIndex);
            }
        }
        console.log('after:');
        console.log(scores);

        collection.update({ _id: doc._id }, { $set: { scores: scores } }, {}, function(err, saved) {
            console.log('Successfully updated ' + saved + ' document');
        });
    };

    function eachCallback(err, doc) {
        if (err) throw err;

        if (doc === null) {
            return db.close();
        }

        removeLowestHomework(doc);

    };

    cursor.each(eachCallback);
    console.log("end");
});

